import Game from "../behavior/Game";

const NAMES = {
    CREATE_GAME: "CREATE_GAME"
}

export const createGame = (namePlayer1, namePlayer2, nplays = 3) => {
    return {
        type: NAMES.CREATE_GAME,
        game: new Game({name: namePlayer1}, {name: namePlayer2}, nplays)
    }
}

export default NAMES;
