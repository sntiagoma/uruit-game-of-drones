import ACTIONS from '../actions';

const game = (state={}, action) => {
    switch(action.type){
        case ACTIONS.CREATE_GAME:
            return {
                ...state,
                game: action.game
            };
        default:
            return state;
    }
}

export default game;