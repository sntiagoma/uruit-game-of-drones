import React from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import App from './components/App';
import NotFound from './components/NotFound';
import Game from './components/Game';
import Scores from './components/Scores';
const Routes = props => (
    <Router>
        <div>
            {/*<ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/scores">Scores</Link></li>
                <li><Link to="/notfound">Topics</Link></li>
            </ul>*/}
            <Switch>
                <Route path="/start" component={App}/>
                <Route path="/scores" component={Scores}/>
                <Route path="/game" component={Game}/>
                <Redirect exact from="/" to="/start" />
                <Route component={NotFound}/>
            </Switch> 
        </div>
    </Router>
)

export default Routes;