import Game,{Movements} from "./Game"

describe("Movements", () => {
    const paper = Movements.moves.find(e=> e.move === 'paper');
    const rock = Movements.moves.find(e=> e.move === 'rock');
    const scissors = Movements.moves.find(e=> e.move === 'scissors');
    it("Should win", ()=>{
        expect(Movements.vs(paper.move, rock.move)).toBe('paper');
        expect(Movements.vs(rock.move, scissors.move)).toBe('rock');
        expect(Movements.vs(scissors.move, paper.move)).toBe('scissors');
    })
    it("Should lose", ()=>{
        expect(Movements.vs(paper.move, scissors.move)).toBe('scissors');
        expect(Movements.vs(rock.move, paper.move)).toBe('paper');
        expect(Movements.vs(scissors.move, rock.move)).toBe('rock');
    })
    it("Should tie", ()=>{
        expect(Movements.vs(paper.move, paper.move)).toBe('tie');
        expect(Movements.vs(rock.move, rock.move)).toBe('tie');
        expect(Movements.vs(scissors.move, scissors.move)).toBe('tie');
        expect(Movements.vs(scissors.move, 'batman')).toBe('tie');
        expect(Movements.vs('superman', 'batman')).toBe('tie');
    })
})

describe("Game", () => {
    it("Should win player1", () => {

        const status = () => {
            //console.log("st:", game.status, `<${game.nplayer1},${game.nplayer2}>`,'\n', game.plays);
        }

        const game = new Game({name: 'one'}, {name: 'two'});
        status();        
        game.play("paper", "rock");
        status();
        expect(game.status.ended).toBeFalsy();
        expect(game.status.winner).toBe('one');
        game.play("paper", "rock");
        status();
        expect(game.status.ended).toBeFalsy();
        expect(game.status.winner).toBe('one');
        game.play("paper", "rock");
        status();
        expect(game.status.ended).toBeTruthy();
        expect(game.status.winner).toBe('one');
    })
})