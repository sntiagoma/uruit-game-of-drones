export class Player {
    contructor(name){
        this.name = name;
    }
}

export class Movements {
    static moves = [
        {move: 'paper', kills: 'rock'},
        {move: 'rock', kills: 'scissors'},
        {move: 'scissors', kills: 'paper'}
    ];
    static vs(mov1name, mov2name){
        const mov1 = Movements.moves.find(e => e.move === mov1name);
        const mov2 = Movements.moves.find(e => e.move === mov2name);
        if(mov1 && mov2) {
            if(mov1.kills === mov2.move) {return mov1.move}
            else if(mov2.kills === mov1.move) {return mov2.move}
            else {return 'tie'}
        }else {
            return 'tie'
        }      
    }
}

export default class Game {
    constructor(player1, player2, nplays = 3){
        this.plays = [];
        this.nplays = nplays;
        this.player1 = player1;
        this.player2 = player2;
    }
    
    play(player1mov, player2mov){
        let {ended} = this.status;
        if(ended){
            //throw new Error("Game ended, no more movements availiable");
        }
        let winner;
        switch(Movements.vs(player1mov, player2mov)){
            case player1mov:
                winner = this.player1.name;
                break;
            case player2mov:
                winner = this.player2.name;
                break;
            case 'tie':
            default:
                winner = 'tie'
        }
        this.plays.push({
            player1mov,
            player2mov,
            winner
        })
        return this;
    }

    get nplayer1(){
        return this.plays.filter(e => e.winner === this.player1.name).length;
    }

    get nplayer2(){
        return this.plays.filter(e => e.winner === this.player2.name).length;
    }

    get status(){
        if(this.nplayer1 >= this.nplays){
            return {
                ended: true,
                winner: this.player1.name
            };
        }else if(this.nplayer2 >= this.nplays){
            return {
                ended: true,
                winner: this.player2.name
            };
        }else {
            let winner;
            if(this.nplayer1>this.nplayer2){ winner = this.player1.name }
            else if(this.nplayer2>this.nplayer1) { winner = this.player2.name }
            else {winner = 'tie'}
            return {
                ended: false,
                winner: winner
            }
        }
    }

}