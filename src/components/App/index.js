import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import classnames from 'classnames';
import connectProps from '../connect-props';

import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    player1name: "",
    player2name: "",
    nplays: 3
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  get enabled(){
    return this.state.player1name &&
      this.state.player2name &&
      (this.state.player1name !== this.state.player2name) &&
      this.state.player1name !== 'tie' &&
      this.state.player2name !== 'tie';
  }

  start = event => {
    if(this.enabled){
      this.props.startgame(this.state.player1name, this.state.player2name, Math.floor(this.state.nplays));
      this.props.history.push("/game");
    }
  }

  render() {
    const { className } = this.props;
    return (
      <div className={classnames("App", className)}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Game of Drones</h1>
        </header>
        <p className="App-intro">
          To get started please input the user names
        </p>
        <div className="player-form">
          <p className={"vs "+(this.enabled?'displayed':'')}>
              {this.state.player1name} vs {this.state.player2name}
          </p>
          <div className="player-form-row">
            <label htmlFor="player1">Player 1:</label>
            <input id="player1" type="text" placeholder="Name"
              name="player1name" onChange={this.handleInputChange}/>
          </div>
          <div className="player-form-row">
            <label htmlFor="player2">Player 2:</label>
            <input id="player2" type="text" placeholder="Name"
              name="player2name" onChange={this.handleInputChange}/>
          </div>
          <div className="player-form-row">
            A player wins with <input type="number" name="nplays" value={this.state.nplays}
              min="1" step="1" max="99" onChange={this.handleInputChange}/> rounds
          </div>
          <div className="player-form-row">
            <button
              onClick={this.start}
              disabled={ (this.enabled)?false:true}>
              Start
            </button>
            <p className="scores">
              <Link to="/scores">Scores</Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default connectProps(App);
