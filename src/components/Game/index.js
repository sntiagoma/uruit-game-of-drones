import React,{Component, PropTypes} from 'react';
import {Link} from 'react-router-dom';
import connectProps from '../connect-props';
import classnames from 'classnames';
import axios from 'axios';
import Board from "../Board";

import './style.css';
import rimage from '../../images/gameofdrones.png';

import paper from '../../images/paper.png';
import rock from '../../images/rock.png';
import scissors from '../../images/scissors.png';
import defaultimg from '../../images/default.png';

import {Movements} from "../../behavior/Game";

const icons = {
    paper,
    rock,
    scissors,
    default: defaultimg
}

const getIcon = name => {
    if(icons[name]){return icons[name]}
    else{ return icons['default']}
}


const Option = props => <div className="game-select-option">
    <div className="container-one-one" onClick={props.onClick}>
        <div className={"container-content"}>
            <img src={getIcon(props.type)} alt={props.type}/>
        </div>
    </div>
    <div className="game-select-option-name">{props.type}</div>
</div>

const GameSelect = props => {
    const options = Movements.moves.map(e => e.move).map(
        name => <Option key={name} type={name}
            onClick={props.move(props.player, name)}/>
    )

    return <div className={classnames("column", props.className)}>
        <div className="player-name">
            {props.player}
        </div>
        <div className="game-select">
            <div className="game-select-title">
                {props.selected?props.selected:'Choose an option'}
            </div>
            <div className="game-select-options">
                {options}
            </div>
        </div>
    </div>
}

class Game extends Component {
    constructor(props){
        super(props);
        console.log("create", props);
        this.state = {
            player1mov: "",
            player2mov: "",
            sent: false
        };
        if(props.game){
            this.state.game = props.game;
            console.log("Game exits")
        }else {
            this.props.history.push("/")
            //redirect to /start
            //console.log("no exists", props);
            //this.state.game =  props.startgame('x','y');
        }
    }

    player1click = (player, movement) => e => {
        if(this.state.player2mov){
            this.setState({
                player1mov: "",
                player2mov: "",
                game: this.props.game.play(movement, this.state.player2mov)
            })
        }else {
            this.setState({
                player1mov: movement
            })
        }
    }

    player2click = (player, movement) => e => {  
        if(this.state.player1mov){
            this.setState({
                player1mov: "",
                player2mov: "",
                game: this.props.game.play(this.state.player1mov, movement)
            })
        }else {
            this.setState({
                player2mov: movement
            })
        }
    }

    componentDidUpdate(prevProps, prevState){
        console.log("cdidu", prevState.game.status.ended, this.state.sent );
        if(prevState.game.status.ended && !this.state.sent){
            axios.post("/api/matches", this.state.game).then( resp => {
                console.log("Added", resp.data);
            }).catch(console.error);
            this.setState({
                sent: true
            });
        }
    }

    render(){
        const {className} = this.props;

        const title = this.state.game ? (
            <div className="game-names">
                {this.state.game.player1.name} &lt;{this.state.game.nplayer1}&gt;
                vs&nbsp;
                &lt;{this.state.game.nplayer2}&gt; {this.state.game.player2.name}
            </div>
        ) : '';

        const winner = this.state.game ? (
            <div className={"game-winner "+this.state.game.status.winner}>
                {this.state.game.status.winner}
            </div>
        ) : '';

        const status = this.state.game ? (
            this.state.game.status.ended ?
            (<div>Close</div>) :
            (<div>Playing...</div>)
        ) : '';

        const board = (this.state.game && !this.state.game.status.ended) ? (
            <div className="game-board row center-xs">
                <GameSelect player={this.state.game.player1.name}
                    move={this.player1click}
                    selected={this.state.player1mov}
                    className="col-xs-12 col-md-6"/>
                <GameSelect player={this.state.game.player2.name}
                    move={this.player2click}
                    selected={this.state.player2mov}
                    className="col-xs-12 col-md-6"/>
            </div>
        ): <div className="row center-xs">
            <div className="col-xs-12" style={{
                textTransform: 'uppercase',
                fontSize: '2rem'
            }}>
                {winner} Wins!
            </div>
            <div className="col-xs-12">
                <Link className="btn" to="/">New Game</Link>
            </div>
            <div className="col-xs-12">
                <Link className="btn" to="/scores">Scores</Link>
            </div>
        </div>;

        const results = this.state.game?<Board game={this.state.game}/>:'';

        return (
            <div className={classnames('Game', className)}>
                <div className="status-bar">
                    <div className="game-label">Game:</div>
                    {title}
                    {winner}
                    {status}
                </div>
                <div className="game-grid row">
                    <div className="left col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <div className="img-r">
                            <img src={rimage} className="animated-flip" alt="Header"/>
                        </div>
                        {board}
                    </div>
                    <div className="right col-xs-12 col-sm-12 col-md-5 col-lg-4">{results}</div>
                </div>
            </div>
        );
    }
} 

export default connectProps(Game);