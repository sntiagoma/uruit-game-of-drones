import React from 'react';
import {configure, shallow, mount, render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Game from "../../behavior/Game";
import Board from '../Board';

configure({adapter: new Adapter()});

describe('<Board/>', () => {
    it('should compute results', ()=>{
        const game = new Game({name:'x'}, {name: 'y'}, 3);
        const wrapper = shallow(<Board game={game}/>);
        expect(wrapper.find('tfoot tr td').at(0).text()).toBe('Total');
        expect(wrapper.find('tfoot tr td').at(1).text()).toBe('Wins: 0');
        expect(wrapper.find('tfoot tr td').at(2).text()).toBe('Wins: 0');
        expect(wrapper.find('tfoot tr td').at(3).text()).toBe('Winner: tie');
    });
    it('should win x', ()=>{
        const game = (new Game({name:'x'}, {name: 'y'}, 3))
            .play("paper", "rock")
            .play("paper", "rock");
        const wrapper = shallow(<Board game={game}/>);
        expect(wrapper.find('tfoot tr td').at(0).text()).toBe('Total');
        expect(wrapper.find('tfoot tr td').at(1).text()).toBe('Wins: 2');
        expect(wrapper.find('tfoot tr td').at(2).text()).toBe('Wins: 0');
        expect(wrapper.find('tfoot tr td').at(3).text()).toBe('Winner: x');
    });
    it('should win y', ()=>{
        const game = (new Game({name:'x'}, {name: 'y'}, 3))
            .play("rock", "paper")
            .play("paper", "rock")
            .play("rock", "paper");
        const wrapper = shallow(<Board game={game}/>);
        expect(wrapper.find('tfoot tr td').at(0).text()).toBe('Total');
        expect(wrapper.find('tfoot tr td').at(1).text()).toBe('Wins: 1');
        expect(wrapper.find('tfoot tr td').at(2).text()).toBe('Wins: 2');
        expect(wrapper.find('tfoot tr td').at(3).text()).toBe('Winner: y');
    });
})