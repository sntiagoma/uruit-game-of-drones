import React from 'react';

import "./style.css";

export default props => {
    const {game} = props;
    const player1wins = game.plays.filter(e => (e.winner === game.player1.name)).length;
    const player2wins = game.plays.filter(e => (e.winner === game.player2.name)).length;
    return <div className="Board">
        <div className="board-header">
            <div className="player">
                {game.player1.name} vs {game.player2.name} wins the 1st to make {game.nplays}
            </div>
        </div>
        <table>
            <thead>
                <tr>
                    <th> round </th>
                    <th> {game.player1.name} </th>
                    <th> {game.player2.name} </th>
                    <th> winner </th>
                </tr>
            </thead>
            <tbody>
                {game.plays.map((e,i) => <tr key={i}>
                        <td># {i}</td>
                        <td>{e.player1mov}</td>
                        <td>{e.player2mov}</td>
                        <td>{e.winner}</td>
                    </tr>)}
            </tbody>
            <tfoot>
                <tr>
                    <td>Total</td>
                    <td>Wins: {player1wins}</td>
                    <td>Wins: {player2wins}</td>
                    <td>Winner: {player1wins>player2wins?game.player1.name:(player2wins>player1wins)?game.player2.name:'tie'}</td>
                </tr>
            </tfoot>
        </table>
    </div>
}