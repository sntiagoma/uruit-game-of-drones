import {connect} from 'react-redux';
import {createGame} from '../actions'

const mapStateToProps = state => {
    return {
      game: state.game
    }
  }
  
const mapDispatchToProps = dispatch => {
    return {
      startgame: (namePlayer1, namePlayer2, nplays = 3) => {
        const action = createGame(namePlayer1, namePlayer2, nplays);
        dispatch(action);
        return action.game;
      }
    }
}

export default (classx) => {
    return connect(mapStateToProps, mapDispatchToProps)(classx);
}