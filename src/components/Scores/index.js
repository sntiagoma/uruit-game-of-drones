import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Board from '../Board';

import "./styles.css";

export default class Scores extends Component{
    constructor(props){
        super(props);
        this.state = {
            matches: []
        };
    }
    
    componentWillMount(){
        axios.get('/api/matches')
            .then(res => {
                this.setState({
                    matches: res.data
                })
            })
            .catch(e => console.error(e))
    }
    render(){
        return <div className="Scores row">
            <div className="home col-xs-12 center-xs">
                <Link to="/">Home</Link>
            </div>
            {this.state.matches.map(game => <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={game._id}>
                <Board game={game}/>
            </div>)}
        </div>
    }
}