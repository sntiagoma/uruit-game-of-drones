# Game of Drones


## Disclaimer
> This project was developed with node (v8.6.0) and yarn (v1.1.0) be sure to have same or above versions to avoid issues or configure an enviorement with [nvm](https://github.com/creationix/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows) and then run `nvm install v8.6.0 && nvm alias default 8.6.0 && nvm use default` for node and then `npm install -g yarn` for yarn (It could work with npm as well)

## Run Instructions
After downloaded the package and being in the root directory of the project (same where package.json is shown) run `yarn install --production=true` or `npm instal --production=true` then run `yarn serve` or `npm run serve`.

On your a modern browser go to [http://localhost:8080](http://localhost:8080).

## Dev Instructions
Install all dependencies with `yarn` then start the server with `yarn serve` on a new prompt run the liveserver for the font-end with `yarn start` and your browser will be opened at [http://localhost:3000](http://localhost:3000) you still need running the backend server (the one in :8080) due It'll proxy all request of /api.

## Stack
### Front-end Stack
React + Redux + ReactRouter + Axios
### Back-end Stack
Node.js + Restify + Axios + NeDB
> NeDB was used as database due this is not a final product (not even an alpha or beta) so with an in-memory database problems of database configuration are skipped. As well was selected over other in-memory databases as Lowdb or Leveldb due that NeDB implements an inteface that is a subset of MongoDB.

> In /score there's some preview matches if you want to start without them just delete server/storage/matches.db file without running the server (the file not always reflect real memory status, it's just a backup, so if it's deleted while the server is running it's possible that it just write it again).

### Testing
Jest + Enzyme
> You'll to have a no-production installation to be able to run them. `yarn test` or `npm run test`