const restify = require('restify');
const path    = require('path');
const fs      = require('fs');
const DB      = require('./db');


const server = restify.createServer();
const buildDirectory = path.join(__dirname, '..', 'build');

const {validate} = require("./GameSchema");

server.use(restify.plugins.jsonBodyParser({mapParams: true}));

server.get('/api', function(req, res, next){
    res.send("Hello");
});

server.get('/api/matches', function(req, res, next){
    DB.find({}, function(err, docs){
        if(err) {
            res.status(500).json(err);
        }else{
            res.send(docs);
        }
    })
});

server.post('/api/matches', function(req, res, next){
    if(validate(req.body)){
        DB.insert(req.body, function(err, doc){
            if(err){
                res.status(500);
                res.json(err)
            }else{
                res.json(doc)
            }
        });
    }else{
        res.status(500);
        res.send({
            error: "Data doesn't match with schema",
            data: req.body
        });
    }
})

for(file of ["index.html", "asset-manifest.json", "favicon.ico", "manifest.json", "service-worker.js"]){
    server.get('/'+file, restify.plugins.serveStatic({
        'directory': buildDirectory,
        'default': file
    }))
}

server.get(/\/static\/?.*/, restify.plugins.serveStatic({
    directory: buildDirectory
  }));

server.get('/', restify.plugins.serveStatic({
    'directory': buildDirectory,
    'default': 'index.html'
}));

server.get(/\/?.*/, function(req, res, next){
    fs.readFile(path.join(buildDirectory, 'index.html'), 'utf8', (err, file) => {
        if(err){
            res.send(500);
            return next();
        }else {;
            res.status(200);
            res.set({
                'content-type': 'text/html'
            });
            const html = file.toString()+'\n';
            res.write(html);
            res.end();
            return next();
        }
    });
})

server.listen("8080", function(){
    console.log(`${server.name} listening at ${server.url}`);
});