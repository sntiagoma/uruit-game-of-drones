const NeDB = require('nedb');
const path = require('path');

module.exports = new NeDB({
    filename: path.join(__dirname, "..", "storage", "matches.db"),
    autoload: true
})