const schema = {
    "id": "/Game",
    "type": "object",
    "properties": {
        "plays": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "player1mov": {"type": "string"},
                    "player2mov": {"type": "string"},
                    "winner": {"type": "string"}
                },
                "required": ["player1mov", "player2mov", "winner"] 
            }           
        },
        "nplays": {type: "number"},
        "player1": {
            "type": "object",
            "properties": {
                "name": {"type": "string"}
            },
            "required": ["name"]
        },
        "player2": {
            "type": "object",
            "properties": {
                "name": {"type": "string"}
            },
            "required": ["name"]
        }
    },
    "required": ["player1", "player2", "nplays", "plays"]
}

const validate = obj => (require('jsonschema').validate(obj, schema).errors.length == 0);

module.exports.validate = validate;